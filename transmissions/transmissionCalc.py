import periodictable as pt
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_pdf import PdfPages


def calcTransmission(formula,density,thickness,wavelength):
    '''Calculate transmission of a material
       Density in g/cm^3
       Thickness in cm
       Wavelength in Angstrom
    '''

    material = pt.formula(formula,density=density)
    (SigCoh,SigAbs,SigIncoh) = pt.neutron_scattering(material)[1]
    trans = np.exp(-(SigCoh+SigAbs*wavelength/1.78+SigIncoh)*thickness)

    return trans


if __name__ == '__main__':

    pdfout = PdfPages("WindowTransmissions.pdf")

    nwin = np.arange(1,13, 1)
    materials = ['Si','Al','Ti','Al2O3',"52.5%wt Ti // Zr"] # Description of material using periodictable notation
    winthickness = np.array((1.5,1,0.5,3,0.5))
    densities = [pt.silicon.density,pt.aluminum.density,pt.titanium.density,3.98,5.23]
    wavelengths = np.arange(2.0,36.0,1.0)
    transmissions = np.zeros((len(materials),len(nwin),len(wavelengths)))

    for i,material in enumerate(materials):
        plt.figure(figsize=(11.7,8.3))
        plt.title("Transmission of n x "+str(winthickness[i])+" mm of "+materials[i])
        plt.xlabel("Wavelength (A)")
        plt.ylabel("Transmission")
        for j,windows in enumerate(nwin):
            transmissions[i][j] = calcTransmission(material,densities[i],nwin[j]*winthickness[i]/10.0,wavelengths)
            plt.plot(wavelengths,transmissions[i][j],label="{0} windows = {1} mm".format(nwin[j],winthickness[i]*nwin[j]))
        plt.legend(loc=2,bbox_to_anchor=(1.05,1))
        plt.subplots_adjust(right=0.70)
        plt.ylim((0.0,1.0))
        pdfout.savefig()

    pdfout.close()
    plt.show()
