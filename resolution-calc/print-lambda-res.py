#!/usr/bin/python

import qresolution as qr

lengths = [17.0,18.0,20.0,22.0,25.0,25.5,30.0,30.0,30.6,33.0,37.0,49.0]

for i in lengths:
	print "& {0:.3f} & {1:.3f} & {2:.3f} & {3:.3f}".format(qr.calcLambdaResolution(2.0,i,3.0)/3.4641,qr.calcLambdaResolution(10.0,i,3.0)/3.4641,qr.calcLambdaResolution(2.0,i,3.0),qr.calcLambdaResolution(10.0,i,3.0)/10.0)
	
