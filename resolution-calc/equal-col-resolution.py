#!/usr/bin/python


import numpy as np
from matplotlib import pyplot as plt

import qresolution as qr

from matplotlib.backends.backend_pdf import PdfPages
pp = PdfPages('resolution-equal-collimation.pdf')

#Calc for loki
#Three detectors:
minrad = [0.5, 0.2, 0.044]
maxrad = [2.12, 1.69, 0.35]	
pixelsize = [0.008,0.008,0.002]
l1 = [5.0,5.0,5.0]
l2 = [2.0,5.0,5.5]

SSD = 20.0
R1 = 0.01
R2 = 0.005

wavelength = 2.0
pLen = 3.0

loki_radii = np.array([np.arange(minrad[0],maxrad[0],pixelsize[0]*1.44),
				  np.arange(minrad[1],maxrad[1],pixelsize[1]*1.44),
				  np.arange(minrad[2],maxrad[2],pixelsize[2]*1.44)])

loki_qvals = np.array([ qr.calcQ(wavelength,l2[0],loki_radii[0]),
				   qr.calcQ(wavelength,l2[1],loki_radii[1]),
				   qr.calcQ(wavelength,l2[2],loki_radii[2])])
					
loki_lres = np.array([qr.calcLambdaResolution(wavelength,SSD+l2[0],pLen),
				 qr.calcLambdaResolution(wavelength,SSD+l2[1],pLen),
				 qr.calcLambdaResolution(wavelength,SSD+l2[2],pLen)])
				 
loki_qres = np.array([qr.calcQResolution(R1,R2,l1[0],l2[0],pixelsize[0],loki_radii[0],loki_lres[0]),
				 qr.calcQResolution(R1,R2,l1[0],l2[1],pixelsize[1],loki_radii[1],loki_lres[1]),
				 qr.calcQResolution(R1,R2,l1[0],l2[2],pixelsize[2],loki_radii[2],loki_lres[2])])


plt.figure(figsize=(11.7,8.3))
#Loki
plt.semilogx(loki_qvals[0],loki_qres[0], "r-.", label="LoKI Detector 1 @ {0} m".format(SSD+l2[0]))
plt.semilogx(loki_qvals[1],loki_qres[1], "r--", label="LoKI Detector 2 @ {0} m".format(SSD+l2[1]))
plt.semilogx(loki_qvals[2],loki_qres[2], "r-",label="LoKI Detector 3 @ {0} m".format(SSD+l2[2]))



#Calc for SKADI
minrad = [0.1, 0.25, 0.04]
maxrad = [0.72, 0.72, 0.35]	
pixelsize = [0.008,0.005,0.002]
l1 = [4.0,4.0,4.0]
l2 = [1.0,4.0,4.0]

SSD = 29.0
R1 = 0.01
R2 = 0.005

wavelength = 2.0
pLen = 3.0

skadi_radii = np.array([np.arange(minrad[0],maxrad[0],pixelsize[0]*1.44),
				  np.arange(minrad[1],maxrad[1],pixelsize[1]*1.44),
				  np.arange(minrad[2],maxrad[2],pixelsize[2]*1.44)])

skadi_qvals = np.array([ qr.calcQ(wavelength,l2[0],skadi_radii[0]),
				   qr.calcQ(wavelength,l2[1],skadi_radii[1]),
				   qr.calcQ(wavelength,l2[2],skadi_radii[2])])
					
skadi_lres = np.array([qr.calcLambdaResolution(wavelength,SSD+l2[0],pLen),
				 qr.calcLambdaResolution(wavelength,SSD+l2[1],pLen),
				 qr.calcLambdaResolution(wavelength,SSD+l2[2],pLen)])
				 
skadi_qres = np.array([qr.calcQResolution(R1,R2,l1[0],l2[0],pixelsize[0],skadi_radii[0],skadi_lres[0]),
				 qr.calcQResolution(R1,R2,l1[0],l2[1],pixelsize[1],skadi_radii[1],skadi_lres[1]),
				 qr.calcQResolution(R1,R2,l1[0],l2[2],pixelsize[2],skadi_radii[2],skadi_lres[2])])

#SKADI
plt.semilogx(skadi_qvals[0],skadi_qres[0], "b-.",label="SKADI Detector 1 @ {0} m".format(SSD+l2[0]))
plt.semilogx(skadi_qvals[1],skadi_qres[1], "b--",label="SKADI Detector 2 @ {0} m".format(SSD+l2[1]))
plt.semilogx(skadi_qvals[2],skadi_qres[2], "b-",label="SKADI Detector 3 @ {0} m".format(SSD+l2[2]))


#Calc for Compact
minrad = [0.04]
maxrad = [2.0]	
pixelsize = [0.002]
l1 = [4.0]
l2 = [4.0]

SSD = 16.0
R1 = 0.01
R2 = 0.005

wavelength = 2.0
pLen = 3.0

compact_radii = np.array([np.arange(minrad[0],maxrad[0],pixelsize[0]*1.44)])

compact_qvals = np.array([ qr.calcQ(wavelength,l2[0],compact_radii[0])])
					
compact_lres = np.array([qr.calcLambdaResolution(wavelength,SSD+l2[0],pLen)])
				 
compact_qres = np.array([qr.calcQResolution(R1,R2,l1[0],l2[0],pixelsize[0],compact_radii[0],compact_lres[0])])


#Compact
plt.semilogx(compact_qvals[0],compact_qres[0], "g-",label="Compact SANS Detector @ {0} m".format(SSD+l2[0]))

#Calc for Compact
minrad = [0.04]
maxrad = [2.0]	
pixelsize = [0.002]
l1 = [4.0]
l2 = [4.0]

SSD = 16.0
R1 = 0.01
R2 = 0.005

wavelength = 3.0
pLen = 3.0

compact_radii = np.array([np.arange(minrad[0],maxrad[0],pixelsize[0]*1.44)])

compact_qvals = np.array([ qr.calcQ(wavelength,l2[0],compact_radii[0])])
					
compact_lres = np.array([qr.calcLambdaResolution(wavelength,SSD+l2[0],pLen)])
				 
compact_qres = np.array([qr.calcQResolution(R1,R2,l1[0],l2[0],pixelsize[0],compact_radii[0],compact_lres[0])])


#Compact
plt.semilogx(compact_qvals[0],compact_qres[0], "g--",label="Compact SANS Detector @ {0} m 3A".format(SSD+l2[0]))



plt.title("{0} vs Q for lambda = {1} {2}\n \"Equivalent\" collimation".format(r'$\sigma_Q/Q$',wavelength,r'$\AA$'))
plt.legend(fontsize="small")
plt.xlabel(r'Q ($\AA^{-1}$)')
plt.ylabel(r'$\sigma_Q/Q$')
plt.ylim(0.0,0.25)


pp.savefig(figure=1)
pp.close()
plt.show()
