
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec

import qresolution as qr



class ResolutionComponents():
	
	
	def __init__(self,ssd=22.0,r1=0.01,r2=0.005,minrad=[0.5, 0.2, 0.1],
					maxrad = [2.12, 1.69, 0.35],
					pixelsize = [0.005,0.005,0.002],
					l1 = [5.0,5.0,5.0],
					l2 = [2.0,5.0,5.5],
					wavelengths = [2.0,4.0,10.0,20.0]):
		#Calc for loki
		#Three detectors:
		self.minrad = minrad
		self.maxrad = maxrad	
		self.pixelsize = pixelsize
		self.l1 = l1
		self.l2 = l2

		self.SSD = ssd
		self.R1 = r1
		self.R2 = r2

		self.wavelengths = wavelengths
		self.pLen = 2.86
		
		self.radii = np.array()
		self.qvals = np.zeros((len(wavelengths),len(pixelsize)))
		self.lres = np.zeros((len(wavelengths),len(pixelsize)))
		self.qres = np.zeros((len(wavelengths),len(pixelsize)))
		self.geom_res = np.zeros((len(wavelengths),len(pixelsize)))
		self.det_res = np.zeros((len(wavelengths),len(pixelsize)))
		self.l_res = np.zeros((len(wavelengths),len(pixelsize)))

		print np.shape(self.radii)
	
	def doCalcs(self):
		
		for i,wavelength in enumerate(self.wavelengths):
			for j,px in enumerate(self.pixelsize):
				self.radii[i,j] = np.arange(self.minrad[j],self.maxrad[j],self.pixelsize[j]*1.44)
				self.qvals[i,j] = qr.calcQ(wavelength,self.l2[j],self.radii[i][j])
				self.lres[i,j] = qr.calcLambdaResolution(wavelength,self.SSD+self.l2[j],self.pLen)
			 	self.qres[i,j] = qr.calcQresolution(self.R1,self.R2,self.l1[j],self.l2[j],self.pixelsize[j],self.radii[i][j],self.lres[i][j])
				self.geom_res[i,j] = qr.calcQresolutionComponents(self.R1,self.R2,self.l1[j],self.l2[j],self.pixelsize[j],self.radii[i][j],self.lres[j])[0]
				self.det_res[i,j] = qr.calcQresolutionComponents(self.R1,self.R2,self.l1[j],self.l2[j],self.pixelsize[j],self.radii[i][j],self.lres[i][j])[1]
					
				self.l_res[i,j].fill(qr.calcself.qresolutionComponents(self.R1,self.R2,self.l1[j],self.l2[j],self.pixelsize[j],self.radii[i][j],self.lres[i][j])[2])

	def doPlots(self):
		#Global Q resolution plot
		resfig = plt.figure(figsize=(11.7,8.3))
		resplt = plt.subplot(111)
		
		#Global theta resolution plot
		thetafig = plt.figure(figsize=(11.7,8.3))
		thetaplt = plt.subplot(111)

		for i,wavelength in enumerate(self.wavelengths):
			
			#Global Q resolution plot
			firstline, = resplt.semilogx(self.qvals[i][0],self.qres[i][0],label="Lambda = {0}".format(wavelength))
			resplt.semilogx(self.qvals[i][1],self.qres[i][1], color=firstline.get_color())
			resplt.semilogx(self.qvals[i][2],self.qres[i][2],color=firstline.get_color())

			#Global theta resolution plot
			firstline, = thetaplt.semilogx(self.qvals[i][0],np.sqrt(self.det_res[i][0]*12),label="Lambda = {0}".format(wavelength))
			thetaplt.semilogx(self.qvals[i][1],np.sqrt(self.det_res[i][1]*12),color=firstline.get_color())
			thetaplt.semilogx(self.qvals[i][2],np.sqrt(self.det_res[i][2]*12),color=firstline.get_color())
			
			#Q resolution
			plt.figure(figsize=(8.3,11.7))
			gs = gridspec.GridSpec(2,1)
			plt1 = plt.subplot(gs[0])
			plt2 = plt.subplot(gs[1])
			plt1.semilogx(self.qvals[i][0],self.qres[i][0],label="Det 1")
			plt1.semilogx(self.qvals[i][1],self.qres[i][1],label="Det 2")
			plt1.semilogx(self.qvals[i][2],self.qres[i][2],label="Det 3")
			
			plt1.legend()
			plt1.set_xlabel("Q (A-1)")
			plt1.set_ylabel("Resolution (sigmaQ/Q)")
			## plt1.suptitle("Q resolution sigmaQ/Q at Lambda={0}".format(wavelength))
						
			## Resolution Components
			##plt.figure(figsize=(11.7,8.3))
			
			plt2.semilogx(self.qvals[i][0],self.qres[i][0]/self.qres[i][0], "k-", label="Q Res".format(self.SSD+self.l2[0]))
			plt2.semilogx(self.qvals[i][1],self.qres[i][1]/self.qres[i][1], "k-")
			plt2.semilogx(self.qvals[i][2],self.qres[i][2]/self.qres[i][2], "k-")

			plt2.semilogx(self.qvals[i][0],self.geom_res[i][0]/(self.qres[i][0]**2), "b-", label="Geometric Component (beam size)".format(self.SSD+self.l2[0]))
			plt2.semilogx(self.qvals[i][1],self.geom_res[i][1]/(self.qres[i][1]**2), "b-")
			plt2.semilogx(self.qvals[i][2],self.geom_res[i][2]/(self.qres[i][2]**2), "b-")
			
			plt2.semilogx(self.qvals[i][0],self.det_res[i][0]/(self.qres[i][0]**2), "r-", label="Angular Component (pixel size)".format(self.SSD+self.l2[0]))
			plt2.semilogx(self.qvals[i][1],self.det_res[i][1]/(self.qres[i][1]**2), "r-")
			plt2.semilogx(self.qvals[i][2],self.det_res[i][2]/(self.qres[i][2]**2), "r-")
			
			plt2.semilogx(self.qvals[i][0],self.l_res[i][0]/(self.qres[i][0]**2), "g-", label="Lambda Component (ToF)".format(self.SSD+self.l2[0]))
			plt2.semilogx(self.qvals[i][1],self.l_res[i][1]/(self.qres[i][1]**2), "g-")
			plt2.semilogx(self.qvals[i][2],self.l_res[i][2]/(self.qres[i][2]**2), "g-")
			
			plt2.legend(loc=2,ncol=2,mode='expand',prop={'size':10})
			plt2.set_ylim([0,1.3])
			plt2.set_xlabel("Q (A-1)")
			plt2.set_ylabel("Contribution to Q Resolution")

			plt.suptitle("Resolution and components at Lambda={0}".format(wavelength))

		resplt.legend()
		thetaplt.legend()
		plt.show()

if __name__ == '__main__':
	
	print qr.calcBeamSize(0.005,0.0025,5.0,5.5)
	
	rc = ResolutionComponents(ssd=22.0,r1=0.001,r2=0.001,minrad=[0.5, 0.2, qr.calcBeamSize(0.005,0.0025,5.0,5.5)/2],
					maxrad = [2.12, 1.69, 0.35],
					pixelsize = [0.008,0.008,0.002],
					l1 = [5.0,5.0,5.0],
					l2 = [2.0,5.0,5.5],
					wavelengths = [2.0,4.0,10.0,20.0])
	
	print rc.lres
	rc.doCalcs()
	rc.doPlots()