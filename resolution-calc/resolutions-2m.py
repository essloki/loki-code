
import numpy as np
from matplotlib import pyplot as plt

import qresolution as qr


#Calc for loki
#Three detectors:
minrad = [0.5, 0.2, 0.1]
maxrad = [2.12, 1.69, 0.35]	
pixelsize = [0.008,0.003,0.001]
l1 = [2.0,2.0,2.0]
l2 = [2.0,5.0,5.5]

SSD = 22.0
R1 = 0.01
R2 = 0.005

wavelengths = [2.0]
#wavelengths = [2.0,3.0,4.0,6.0,8.0,10.0,12.0,15.0,20.0]
pLen = 3.0

plt.figure(figsize=(11.7,8.3))

for wavelength in wavelengths:
	loki_radii = np.array([np.arange(minrad[0],maxrad[0],pixelsize[0]*1.44),
					  np.arange(minrad[1],maxrad[1],pixelsize[1]*1.44),
					  np.arange(minrad[2],maxrad[2],pixelsize[2]*1.44)])

	loki_qvals = np.array([ qr.calcQ(wavelength,l2[0],loki_radii[0]),
					   qr.calcQ(wavelength,l2[1],loki_radii[1]),
					   qr.calcQ(wavelength,l2[2],loki_radii[2])])
					
	loki_lres = np.array([qr.calcLambdaResolution(wavelength,SSD+l2[0],pLen),
					 qr.calcLambdaResolution(wavelength,SSD+l2[1],pLen),
					 qr.calcLambdaResolution(wavelength,SSD+l2[2],pLen)])
				 
	loki_qres = np.array([qr.calcQResolution(R1,R2,l1[0],l2[0],pixelsize[0],loki_radii[0],loki_lres[0]),
					 qr.calcQResolution(R1,R2,l1[0],l2[1],pixelsize[1],loki_radii[1],loki_lres[1]),
					 qr.calcQResolution(R1,R2,l1[0],l2[2],pixelsize[2],loki_radii[2],loki_lres[2])])

	loki_geom_res = np.array([qr.calcQResolutionComponents(R1,R2,l1[0],l2[0],pixelsize[0],loki_radii[0],loki_lres[0])[0],
					qr.calcQResolutionComponents(R1,R2,l1[0],l2[1],pixelsize[1],loki_radii[1],loki_lres[1])[0],
					qr.calcQResolutionComponents(R1,R2,l1[0],l2[2],pixelsize[2],loki_radii[2],loki_lres[2])[0]])

	loki_det_res = np.array([qr.calcQResolutionComponents(R1,R2,l1[0],l2[0],pixelsize[0],loki_radii[0],loki_lres[0])[1],
					qr.calcQResolutionComponents(R1,R2,l1[0],l2[1],pixelsize[1],loki_radii[1],loki_lres[1])[1],
					qr.calcQResolutionComponents(R1,R2,l1[0],l2[2],pixelsize[2],loki_radii[2],loki_lres[2])[1]])

	loki_l_res = np.array([np.zeros_like(loki_qvals[0]),np.zeros_like(loki_qvals[1]),np.zeros_like(loki_qvals[2])])
	
	loki_l_res[0].fill(qr.calcQResolutionComponents(R1,R2,l1[0],l2[0],pixelsize[0],loki_radii[0],loki_lres[0])[2])
	loki_l_res[1].fill(qr.calcQResolutionComponents(R1,R2,l1[0],l2[1],pixelsize[1],loki_radii[1],loki_lres[1])[2])
	loki_l_res[2].fill(qr.calcQResolutionComponents(R1,R2,l1[0],l2[2],pixelsize[2],loki_radii[2],loki_lres[2])[2])


	#Loki
	plt.loglog(loki_qvals[0],loki_qres[0], "k-.", label="LoKI Detector 1 @ {0} m".format(SSD+l2[0]))
	plt.loglog(loki_qvals[1],loki_qres[1], "k--", label="LoKI Detector 2 @ {0} m".format(SSD+l2[1]))
	plt.loglog(loki_qvals[2],loki_qres[2], "k-",label="LoKI Detector 3 @ {0} m".format(SSD+l2[2]))

	
	plt.loglog(loki_qvals[0],loki_geom_res[0], "b-.", label="LoKI Detector 1 @ {0} m".format(SSD+l2[0]))
	plt.loglog(loki_qvals[1],loki_geom_res[1], "b--", label="LoKI Detector 2 @ {0} m".format(SSD+l2[1]))
	plt.loglog(loki_qvals[2],loki_geom_res[2], "b-",label="LoKI Detector 3 @ {0} m".format(SSD+l2[2]))

	plt.loglog(loki_qvals[0],loki_det_res[0], "r-.", label="LoKI Detector 1 @ {0} m".format(SSD+l2[0]))
	plt.loglog(loki_qvals[1],loki_det_res[1], "r--", label="LoKI Detector 2 @ {0} m".format(SSD+l2[1]))
	plt.loglog(loki_qvals[2],loki_det_res[2], "r-",label="LoKI Detector 3 @ {0} m".format(SSD+l2[2]))


	plt.loglog(loki_qvals[0],loki_l_res[0], "g-.", label="LoKI Detector 1 @ {0} m".format(SSD+l2[0]))
	plt.loglog(loki_qvals[1],loki_l_res[1], "g--", label="LoKI Detector 2 @ {0} m".format(SSD+l2[1]))
	plt.loglog(loki_qvals[2],loki_l_res[2], "g-",label="LoKI Detector 3 @ {0} m".format(SSD+l2[2]))


plt.show()