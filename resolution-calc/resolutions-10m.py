
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec

import qresolution as qr



class ResolutionComponents():
	
	
	def __init__(self,ssd=22.0,r1=0.01,r2=0.005,minrad=[0.5, 0.2, 0.1],
					maxrad = [2.12, 1.69, 0.35],
					pixelsize = [0.005,0.005,0.002],
					l1 = [5.0,5.0,5.0],
					l2 = [2.0,5.0,5.5],
					wavelengths = [2.0,4.0,10.0,20.0]):
		#Calc for loki
		#Three detectors:
		self.minrad = minrad
		self.maxrad = maxrad	
		self.pixelsize = pixelsize
		self.l1 = l1
		self.l2 = l2

		self.SSD = ssd
		self.R1 = r1
		self.R2 = r2

		self.wavelengths = wavelengths
		self.pLen = 2.86
	
	def doPlots(self):
		
		#Global Q resolution plot
		resfig = plt.figure(figsize=(11.7,8.3))
		resplt = plt.subplot(111)
		
		#Global theta resolution plot
		thetafig = plt.figure(figsize=(11.7,8.3))
		thetaplt = plt.subplot(111)

		
		
		for wavelength in self.wavelengths:

			loki_radii = np.array([np.arange(self.minrad[0],self.maxrad[0],self.pixelsize[0]*1.44),
							  np.arange(self.minrad[1],self.maxrad[1],self.pixelsize[1]*1.44),
							  np.arange(self.minrad[2],self.maxrad[2],self.pixelsize[2]*1.44)])

			loki_qvals = np.array([ qr.calcQ(wavelength,self.l2[0],loki_radii[0]),
							   qr.calcQ(wavelength,self.l2[1],loki_radii[1]),
							   qr.calcQ(wavelength,self.l2[2],loki_radii[2])])
					
			loki_lres = np.array([qr.calcLambdaResolution(wavelength,self.SSD+self.l2[0],self.pLen),
							 qr.calcLambdaResolution(wavelength,self.SSD+self.l2[1],self.pLen),
							 qr.calcLambdaResolution(wavelength,self.SSD+self.l2[2],self.pLen)])
				 
			loki_qres = np.array([qr.calcQResolution(self.R1,self.R2,self.l1[0],self.l2[0],self.pixelsize[0],loki_radii[0],loki_lres[0]),
							 qr.calcQResolution(self.R1,self.R2,self.l1[0],self.l2[1],self.pixelsize[1],loki_radii[1],loki_lres[1]),
							 qr.calcQResolution(self.R1,self.R2,self.l1[0],self.l2[2],self.pixelsize[2],loki_radii[2],loki_lres[2])])

			loki_geom_res = np.array([qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[0],self.pixelsize[0],loki_radii[0],loki_lres[0])[0],
							qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[1],self.pixelsize[1],loki_radii[1],loki_lres[1])[0],
							qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[2],self.pixelsize[2],loki_radii[2],loki_lres[2])[0]])

			loki_det_res = np.array([qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[0],self.pixelsize[0],loki_radii[0],loki_lres[0])[1],
							qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[1],self.pixelsize[1],loki_radii[1],loki_lres[1])[1],
							qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[2],self.pixelsize[2],loki_radii[2],loki_lres[2])[1]])

			loki_l_res = np.array([np.zeros_like(loki_qvals[0]),np.zeros_like(loki_qvals[1]),np.zeros_like(loki_qvals[2])])
	
			loki_l_res[0].fill(qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[0],self.pixelsize[0],loki_radii[0],loki_lres[0])[2])
			loki_l_res[1].fill(qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[1],self.pixelsize[1],loki_radii[1],loki_lres[1])[2])
			loki_l_res[2].fill(qr.calcQResolutionComponents(self.R1,self.R2,self.l1[0],self.l2[2],self.pixelsize[2],loki_radii[2],loki_lres[2])[2])

			#Global Q resolution plot
			firstline, = resplt.semilogx(loki_qvals[0],loki_qres[0],label="Lambda = {0}".format(wavelength))
			resplt.semilogx(loki_qvals[1],loki_qres[1], color=firstline.get_color())
			resplt.semilogx(loki_qvals[2],loki_qres[2],color=firstline.get_color())

			#Global theta resolution plot
			firstline, = thetaplt.semilogx(loki_qvals[0],np.sqrt(loki_det_res[0]*12),label="Lambda = {0}".format(wavelength))
			thetaplt.semilogx(loki_qvals[1],np.sqrt(loki_det_res[1]*12),color=firstline.get_color())
			thetaplt.semilogx(loki_qvals[2],np.sqrt(loki_det_res[2]*12),color=firstline.get_color())
			
			#Q resolution
			plt.figure(figsize=(8.3,11.7))
			gs = gridspec.GridSpec(2,1)
			plt1 = plt.subplot(gs[0])
			plt2 = plt.subplot(gs[1])
			plt1.semilogx(loki_qvals[0],loki_qres[0],label="Det 1")
			plt1.semilogx(loki_qvals[1],loki_qres[1],label="Det 2")
			plt1.semilogx(loki_qvals[2],loki_qres[2],label="Det 3")
			
			plt1.legend()
			plt1.set_xlabel("Q (A-1)")
			plt1.set_ylabel("Resolution (sigmaQ/Q)")
			## plt1.suptitle("Q resolution sigmaQ/Q at Lambda={0}".format(wavelength))
						
			## Resolution Components
			##plt.figure(figsize=(11.7,8.3))
			
			plt2.semilogx(loki_qvals[0],loki_qres[0]/loki_qres[0], "k-", label="Q Res".format(self.SSD+self.l2[0]))
			plt2.semilogx(loki_qvals[1],loki_qres[1]/loki_qres[1], "k-")
			plt2.semilogx(loki_qvals[2],loki_qres[2]/loki_qres[2], "k-")

			plt2.semilogx(loki_qvals[0],loki_geom_res[0]/(loki_qres[0]**2), "b-", label="Geometric Component (beam size)".format(self.SSD+self.l2[0]))
			plt2.semilogx(loki_qvals[1],loki_geom_res[1]/(loki_qres[1]**2), "b-")
			plt2.semilogx(loki_qvals[2],loki_geom_res[2]/(loki_qres[2]**2), "b-")
			
			plt2.semilogx(loki_qvals[0],loki_det_res[0]/(loki_qres[0]**2), "r-", label="Angular Component (pixel size)".format(self.SSD+self.l2[0]))
			plt2.semilogx(loki_qvals[1],loki_det_res[1]/(loki_qres[1]**2), "r-")
			plt2.semilogx(loki_qvals[2],loki_det_res[2]/(loki_qres[2]**2), "r-")
			
			plt2.semilogx(loki_qvals[0],loki_l_res[0]/(loki_qres[0]**2), "g-", label="Lambda Component (ToF)".format(self.SSD+self.l2[0]))
			plt2.semilogx(loki_qvals[1],loki_l_res[1]/(loki_qres[1]**2), "g-")
			plt2.semilogx(loki_qvals[2],loki_l_res[2]/(loki_qres[2]**2), "g-")
			
			plt2.legend(loc=2,ncol=2,mode='expand',prop={'size':10})
			plt2.set_ylim([0,1.3])
			plt2.set_xlabel("Q (A-1)")
			plt2.set_ylabel("Contribution to Q Resolution")

			plt.suptitle("Resolution and components at Lambda={0}".format(wavelength))

		resplt.legend()
		thetaplt.legend()
		plt.show()

if __name__ == '__main__':
	
	rc = ResolutionComponents()
	rc.doPlots()