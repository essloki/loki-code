
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import qresolution as qr



class ResolutionComponents():
	
	def __init__(self,ssd=22.0,r1=0.01,r2=0.005,
					minrad=0.5,
					maxrad = 2.12,
					pixelsize = 0.005,
					l1 = 5.0,
					l2 = 2.0,
					wavelength = 2.0):
		self.wavelength=wavelength
		self.minrad = minrad
		self.maxrad = maxrad	
		self.pixelsize = pixelsize
		self.l1 = l1
		self.l2 = l2

		self.SSD = ssd
		self.R1 = r1
		self.R2 = r2

		self.pLen = 2.86
		
		self.radii = np.arange(self.minrad,self.maxrad,self.pixelsize*1.44)
		self.qvals = qr.calcQ(self.wavelength,self.l2,self.radii)
		self.lres = qr.calcLambdaResolution(self.wavelength,self.SSD+self.l2,self.pLen)
	 	self.qres = qr.calcQResolution(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres)
		self.geom_res = qr.calcQResolutionComponents(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres)[0]
		self.det_res = qr.calcQResolutionComponents(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres)[1]
		
		self.l_res = np.zeros(len(self.qvals))	
		self.l_res.fill(qr.calcQResolutionComponents(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres)[2])

def doPlots(detectors,filename="figs.pdf",saveplots=0):
	#Global Q resolution plot
	resfig = plt.figure(figsize=(11.7,8.3))
	resplt = plt.subplot(111)
	resfig.suptitle("Q resolution\nR1={0},R2={1},L1={2},lambda={3}".format(detectors[0].R1,detectors[0].R2,detectors[0].l1,detectors[0].wavelength))
	resplt.set_xlim([1e-4,10])
	resplt.set_ylim([0,0.40])
	resplt.set_xlabel("Q (A-1)")
	resplt.set_ylabel("sigmaQ/Q")
	
	#Global theta resolution plot
	thetafig = plt.figure(figsize=(11.7,8.3))
	thetaplt = plt.subplot(111)
	thetafig.suptitle("Angular resolution (detector resolution)\nR1={0},R2={1},L1={2},lambda={3}".format(detectors[0].R1,detectors[0].R2,detectors[0].l1,detectors[0].wavelength))
	thetaplt.set_xlim([1e-4,10])
	thetaplt.set_ylim([0,0.40])
	thetaplt.set_xlabel("Q (A-1)")
	thetaplt.set_ylabel("dtheta/theta (== dR/R)")
	
	#Components plot
	compfig = plt.figure(figsize=(11.7,8.3))
	compplt = plt.subplot(111)
	compfig.suptitle("Resolution Components\nR1={0},R2={1},L1={2},lambda={3}".format(detectors[0].R1,detectors[0].R2,detectors[0].l1,detectors[0].wavelength))
	compplt.set_ylim([0,1.1])
	compplt.set_xlabel("Q (A-1)")
	compplt.set_ylabel("Contribution to Q Resolution")
	
	
	for i,detector in enumerate(detectors):
		
		#Global Q resolution plot
		resplt.semilogx(detector.qvals,detector.qres, label="L2={0}, px={1}".format(detector.l2,detector.pixelsize))

		#Global theta resolution plot
		thetaplt.semilogx(detector.qvals,np.sqrt(detector.det_res*12),label="L2={0}, px={1}".format(detector.l2,detector.pixelsize))

							
		## Resolution Components
		if i == 0:		
			#compplt.semilogx(detector.qvals,detector.qres/detector.qres, "k-", label="Q Res")
			compplt.semilogx(detector.qvals,detector.geom_res/(detector.qres**2), "b-", label="Geometric Component (beam size)")
			compplt.semilogx(detector.qvals,detector.det_res/(detector.qres**2), "r-", label="Angular Component (pixel size)")
			compplt.semilogx(detector.qvals,detector.l_res/(detector.qres**2), "g-", label="Lambda Component (ToF)")
		else:
			#compplt.semilogx(detector.qvals,detector.qres/detector.qres, "k-")
			compplt.semilogx(detector.qvals,detector.geom_res/(detector.qres**2), "b-")
			compplt.semilogx(detector.qvals,detector.det_res/(detector.qres**2), "r-")
			compplt.semilogx(detector.qvals,detector.l_res/(detector.qres**2), "g-")
			
		
		#plt2.set_ylim([0,1.3])
		#plt2.set_xlabel("Q (A-1)")
		#plt2.set_ylabel("Contribution to Q Resolution")

		#plt.suptitle("Resolution and components at Lambda={0}".format(wavelength))

	resplt.legend()
	thetaplt.legend()

	compplt.legend(loc=2,ncol=2,mode='expand',prop={'size':10})
	if saveplots==1:
		pp = PdfPages(filename)
		pp.savefig(resfig)
		pp.savefig(thetafig)
		pp.savefig(compfig)
		pp.close()
	else:
		plt.show()

if __name__ == '__main__':
	
	#setup instrument values
	ssd=22.0
	r1=0.005
	r2=0.0025
	l1=5.0
	
	print "Beamsize on back detector = {0}".format(qr.calcBeamSize(r1,r2,l1,10.0))
	
	#choose wavelengths
	for wavelength in (2.0,5.0,10.0,20.0):
	
		#calculate each detector
		rc0 = ResolutionComponents(ssd=ssd,r1=r1,r2=r2,minrad=0.5,maxrad=2.12,pixelsize=0.015,l1=l1,l2=2.0,wavelength=wavelength)
		rc1 = ResolutionComponents(ssd=ssd,r1=r1,r2=r2,minrad=0.2,maxrad=1.69,pixelsize=0.008,l1=l1,l2=5.0,wavelength=wavelength)
		rc2 = ResolutionComponents(ssd=ssd,r1=r1,r2=r2,minrad=qr.calcBeamSize(r1,r2,l1,5.5)/2,maxrad=0.35,pixelsize=0.003,l1=l1,l2=5.5,wavelength=wavelength)
		rc3 = ResolutionComponents(ssd=ssd,r1=r1,r2=r2,minrad=qr.calcBeamSize(r1,r2,l1,5.5)/2,maxrad=0.05,pixelsize=0.002,l1=l1,l2=5.5,wavelength=wavelength)		
		doPlots((rc0,rc1,rc2),filename="TG2-resolution-plots-L1-{0}m-{1}A-l.pdf".format(l1,wavelength),saveplots=1)
		
	