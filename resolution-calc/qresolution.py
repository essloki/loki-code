#!/usr/bin/python


import numpy as np



def calcTOF(wavelength,detDist): 
    return wavelength*detDist/3.96

def calcQResolutionComponents(R1,R2,L1,L2,dR,R,dLL):
	''' Calculate Q resolution as per Milder and Carpenter
		Equation taken from Heenan LOQ Resolution document
		
		Returns: (sigmaQ/Q)^2 preceded by geometric(beam size), angular (pixel size) 
		and lambda components of the resolution
		'''
		
	res1a = (R1*L2/(2.0*R*L1))**2.0
	res1b = (R2*(L1+L2)/(2.0*R*L1))**2.0
	res1 = res1a+res1b
	res2 = (1.0/12.0)*(dR/R)**2.0
	res3 = (1.0/12.0)*(dLL)**2.0
	
	return res1,res2,res3,res1+res2+res3

def calcQResolution(R1,R2,L1,L2,dR,R,dLL):
	''' Calculate Q resolution as per Milder and Carpenter
		Equation taken from Heenan LOQ Resolution document
		
		Returns: sigmaQ/Q
		'''
	(res1,res2,res3,res) = calcQResolutionComponents(R1,R2,L1,L2,dR,R,dLL)
	
	return np.sqrt(res)
	
def calcRadiusFromQ(Qval,L2,L):
	''' Calculate the radius on detector
		for detection of scattering at a given Q, 
		SDD and wavelength
		'''
	theta = 2.0*np.arcsin(Qval*L/4/3.1415)	
	
	radius = L2*np.tan(theta)
	
	return radius

def calcLambdaResolution(L,dist,pulseWidth):
	return pulseWidth/calcTOF(L,dist)

def calcQ(L,L2,R):
	return 4*3.1415*np.sin(np.arctan(R/L2)/2)/L

def calcBeamSize(R1,R2,L1,L2):
	''' Returns beam diameter in m'''
	
	diam = 2*R1*L2/L1 + 2*R2*(L1+L2)/L1
	
	return  diam