import numpy as np
from matplotlib import pyplot as plt
from matplotlib import gridspec as gridspec
from matplotlib.backends.backend_pdf import PdfPages
import qresolution as qr

class ResolutionPlot():
	
	def __init__(self,ssd=22.0,r1=0.01,r2=0.005,
					minrad=0.5,
					maxrad = 2.12,
					pixelsize = 0.005,
					l1 = 5.0,
					l2 = 2.0,
					minL = 2.0,
					maxL = 10.5,
					Lsteps = 20):
					
		self.minL=minL
		self.maxL=maxL
		self.Lsteps = Lsteps
		self.minrad = minrad
		self.maxrad = maxrad	
		self.pixelsize = pixelsize
		self.l1 = l1
		self.l2 = l2

		self.SSD = ssd
		self.R1 = r1
		self.R2 = r2

		self.pLen = 2.86

		self.wavelengths = np.arange(self.minL,self.maxL,(self.maxL-self.minL)/self.Lsteps)
		self.radii = np.arange(self.minrad,self.maxrad,self.pixelsize*1.44)		
		self.qvals = np.zeros([len(self.wavelengths),len(self.radii)])
		self.lres = np.zeros([len(self.wavelengths),len(self.radii)])
		self.qres = np.zeros([len(self.wavelengths),len(self.radii)])
		self.geom_res = np.zeros([len(self.wavelengths),len(self.radii)])
		self.det_res = np.zeros([len(self.wavelengths),len(self.radii)])
		
		for i,wavelength in enumerate(self.wavelengths):
			self.qvals[i] = qr.calcQ(wavelength,self.l2,self.radii)
			self.lres[i] = qr.calcLambdaResolution(wavelength,self.SSD+self.l2,self.pLen)
			self.qres[i] = qr.calcQResolution(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres[i])
			self.geom_res[i] = qr.calcQResolutionComponents(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres[i])[0]
			self.det_res[i] = qr.calcQResolutionComponents(self.R1,self.R2,self.l1,self.l2,self.pixelsize,self.radii,self.lres[i])[1]
		
		
def doPlots(detectors,filename="figs.pdf",saveplots=0):
	#Global Q resolution plot
	resfig = plt.figure(figsize=(11.7,8.3))
	resplt = plt.subplot(111)
	resfig.suptitle("Q resolution\nR1={0},R2={1},L1={2}".format(detectors[0].R1,detectors[0].R2,detectors[0].l1))
	resplt.set_xlim([1e-4,10])
	resplt.set_ylim([0,0.60])
	resplt.set_xlabel("Q (A-1)")
	resplt.set_ylabel("sigmaQ/Q")
	
	
	#Global theta resolution plot
	thetafig = plt.figure(figsize=(11.7,8.3))
	thetaplt = plt.subplot(111)
	thetafig.suptitle("Angular resolution (detector resolution)\nR1={0},R2={1},L1={2}".format(detectors[0].R1,detectors[0].R2,detectors[0].l1))
	thetaplt.set_xlim([1e-4,10])
	thetaplt.set_ylim([0,0.60])
	thetaplt.set_xlabel("Q (A-1)")
	thetaplt.set_ylabel("dtheta/theta (== dR/R)")

	for j, wavelength in enumerate(detectors[0].wavelengths):
		for i,detector in enumerate(detectors):
			if i == 0:
				#Global Q resolution plot
				rfp, = resplt.semilogx(detector.qvals[j],detector.qres[j], label="Lmda={0}".format(wavelength))
				tfp, = thetaplt.semilogx(detector.qvals[j],np.sqrt(detector.det_res[j]*12),label="L2={0}, px={1}".format(detector.l2,detector.pixelsize))
			else:
				#Global Q resolution plot
				resplt.semilogx(detector.qvals[j],detector.qres[j], color=rfp.get_color())
				thetaplt.semilogx(detector.qvals[j],np.sqrt(detector.det_res[j]*12),color=tfp.get_color())
				
	resplt.legend()
	#thetaplt.legend()
	if saveplots==1:
		pp = PdfPages(filename)
		pp.savefig(resfig)
		pp.close()
	else:
		plt.show()

if __name__ == '__main__':
	
	#setup instrument values
	ssd=32.5
	r1=0.01
	r2=0.005
	l1=20.0
	
	print "Beamsize on back detector = {0}".format(qr.calcBeamSize(r1,r2,l1,10.0))
	
	#calculate each detector
	rc0 = ResolutionPlot(ssd=ssd,r1=r1,r2=r2,minrad=0.5,maxrad=2.12,pixelsize=0.005,l1=l1,l2=2.0)
	rc1 = ResolutionPlot(ssd=ssd,r1=r1,r2=r2,minrad=0.2,maxrad=1.69,pixelsize=0.005,l1=l1,l2=5.0)
	rc2 = ResolutionPlot(ssd=ssd,r1=r1,r2=r2,minrad=qr.calcBeamSize(r1,r2,l1,5.5)*1.5/2,maxrad=0.35,pixelsize=0.002,l1=l1,l2=20.0)
	
	doPlots((rc0,rc1,rc2),saveplots=0)