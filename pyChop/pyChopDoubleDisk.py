#!/usr/bin/python -u

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.gridspec as gridspec
plt.ioff()

class chopperSystem():
    
    def __init__(self,minL=3.0,maxL=11.0,pulseSkip=0,
                    chop1Dist=6.5,chop1Open=120,chop2Dist=9.8,chop2Open=160,
                    collDist=10.0,rearDist=10.0):
        #Source Settings
        self.pulseSkip = pulseSkip
        self.freq = 14/(1+self.pulseSkip)
        self.numPulses = 2*(1+self.pulseSkip)
        #Wavelength Settings
        self.minL = minL
        self.maxL = maxL
        #Chopper Settings
        self.chopperDiskSep = 0.1
        #Chopper 1 Settings
        self.chopper1Dist = chop1Dist
        self.chopper1Opening = chop1Open
        #Chopper 2 Settings
        self.chopper2Dist = chop2Dist
        self.chopper2Opening = chop2Open
        #Detector Settings
        self.collStartDist = collDist 
        self.samplePosDist = self.collStartDist + 10.0
        self.frontDetDist = self.samplePosDist + 2.0
        self.midDetDist = self.samplePosDist + 5.0
        self.rearDetDist = self.samplePosDist + rearDist

        self.outWidth = 11.7
        self.outHeight = 8.3
        self.oWS = 1/self.outWidth
        self.oHS = 1/self.outHeight
        
        self.chopper1ADist = self.chopper1Dist-self.chopperDiskSep/2.0
        self.chopper1BDist = self.chopper1Dist+self.chopperDiskSep/2.0
        self.chopper2ADist = self.chopper2Dist-self.chopperDiskSep/2.0
        self.chopper2BDist = self.chopper2Dist+self.chopperDiskSep/2.0
    
    
    def doChopperCalcs(self):
        #Chopper 1
        ## First Disk
        self.chopper1AOpen,self.chopper1AClose = self.calcChopperDisk1(self.chopper1ADist,self.minL,self.chopper1Opening,self.freq)
        self.chopper1AX1 = np.array([0,self.chopper1AOpen])
        self.chopper1AX2 = np.array([self.chopper1AClose,self.chopper1AOpen+71.4*(1+self.pulseSkip)])
        self.chopper1AY = np.array([self.chopper1ADist,self.chopper1ADist])
        #print "Chopper 1A: Open = {0} ms; Close = {1} ms\n".format(chopper1AOpen, chopper1AClose)
        ## Second Disk
        self.chopper1BOpen,self.chopper1BClose = self.calcChopperDisk2(self.chopper1BDist,self.maxL,self.chopper1Opening,self.freq)
        if (self.chopper1BOpen > 0.0):
            self.chopper1BX1 = np.array([0,self.chopper1BOpen])
        else:
            self.chopper1BX1 = np.array([0,0])
        
        self.chopper1BX2 = np.array([self.chopper1BClose,self.chopper1BOpen+71.4*(1+self.pulseSkip)])
        self.chopper1BY = np.array([self.chopper1BDist,self.chopper1BDist])
        #print "Chopper 1B: Open = {0} ms; Close = {1} ms\n".format(chopper1BOpen, chopper1BClose)
        
        self.chopper2AOpen,self.chopper2AClose = self.calcChopperDisk1(self.chopper2ADist,self.minL,self.chopper2Opening,self.freq)
        self.chopper2AX1 = np.array([0,self.chopper2AOpen])
        self.chopper2AX2 = np.array([self.chopper2AClose,self.chopper2AOpen+71.4*(1+self.pulseSkip)])
        self.chopper2AY = np.array([self.chopper2ADist,self.chopper2ADist])
        #print "Chopper 1A: Open = {0} ms; Close = {1} ms\n".format(chopper2AOpen, chopper2AClose)
        ## Second Disk
        self.chopper2BOpen,self.chopper2BClose = self.calcChopperDisk2(self.chopper2BDist,self.maxL,self.chopper2Opening,self.freq)
        if (self.chopper2BOpen > 0.0):
            self.chopper2BX1 = np.array([0,self.chopper2BOpen])
        else:
            self.chopper2BX1 = np.array([0,0])
        
        self.chopper2BX2 = np.array([self.chopper2BClose,self.chopper2BOpen+71.4*(1+self.pulseSkip)])
        self.chopper2BY = np.array([self.chopper2BDist,self.chopper2BDist])
        #print "Chopper 1B: Open = {0} ms; Close = {1} ms\n".format(chopper2BOpen, chopper2BClose)
    
    
    def doLambdaCalcs(self):
        self.minLPen = self.calcLambda(2.86,self.chopper2AOpen,self.chopper2ADist)
        self.maxLPen = self.calcLambda(0,self.chopper2BClose,self.chopper2BDist)
            
        self.minLX = np.array([0,self.calcTOF(self.minL,self.rearDetDist)])
        self.minLY = np.array([0,self.rearDetDist])
        
        self.maxLX = np.array([2.86,2.86+self.calcTOF(self.maxL,self.rearDetDist)])
        self.maxLY = np.array([0,self.rearDetDist])
        
        self.minLPenX = np.array([2.86,2.86+self.calcTOF(self.minLPen,self.rearDetDist)])
        self.minLPenY = np.array([0,self.rearDetDist])
        
        self.maxLPenX = np.array([0,self.calcTOF(self.maxLPen,self.rearDetDist)])
        self.maxLPenY = np.array([0,self.rearDetDist])
    
    def doTDPlot(self):
        fig = plt.figure(figsize=(self.outWidth,self.outHeight))
        gs = gridspec.GridSpec(1,2, width_ratios=[4,1]) #,height_ratios=[20,1])
        plt0=plt.subplot(gs[0])
        
        #Draw choppers
        plt0.plot(self.chopper1AX1,self.chopper1AY, 'k',self.chopper1AX2,self.chopper1AY, 'k', self.chopper1AX2+71.4*(1+self.pulseSkip),self.chopper1AY, 'k')
        plt0.plot(self.chopper1BX1,self.chopper1BY, 'k',self.chopper1BX2,self.chopper1BY, 'k', self.chopper1BX2+71.4*(1+self.pulseSkip), self.chopper1BY, 'k')
        
        plt0.plot(self.chopper2AX1,self.chopper2AY, 'k',self.chopper2AX2,self.chopper2AY, 'k', self.chopper2AX2+71.4*(1+self.pulseSkip), self.chopper2AY, 'k')
        plt0.plot(self.chopper2BX1,self.chopper2BY, 'k',self.chopper2BX2,self.chopper2BY, 'k', self.chopper2BX2+71.4*(1+self.pulseSkip), self.chopper2BY, 'k')
        
        #Draw neutron trajectories
        plt0.plot(self.minLX,self.minLY, 'r', self.maxLX,self.maxLY, 'b')
        plt0.plot(self.minLX+71.4*(1+self.pulseSkip),self.minLY, 'r', self.maxLX+71.4*(1+self.pulseSkip), self.maxLY, 'b')
        plt0.plot(self.minLPenX,self.minLPenY, 'r--', self.maxLPenX,self.maxLPenY, 'b--')
        plt0.plot(self.minLPenX+71.4*(1+self.pulseSkip),self.minLPenY, 'r--', self.maxLPenX+71.4*(1+self.pulseSkip),self.maxLPenY, 'b--')
        
        ax = plt.gca()
        #Draw pulses
        for pulse in range(self.numPulses):
            ax.add_patch(Rectangle((pulse*71.4,0),2.86,self.rearDetDist, facecolor="lightgrey"))
            #ax.add_patch(Rectangle((71.4,0),2.86,rearDetDist, facecolor="lightgrey"))
        
        #Draw Detectors
        plt0.axhline(self.rearDetDist, lw=1, ls='--', color='g')
        plt0.axhline(self.midDetDist, lw=1, ls='--', color='g')
        plt0.axhline(self.frontDetDist, lw=1, ls='--', color='g')
        #Draw Sample Position
        plt0.axhline(self.samplePosDist, lw=1, ls='-.', color='g')
        
        plt0.set_xlabel('Time (ms)')
        plt0.set_ylabel('Distance (m)')
        
        plt0.set_ylim(ymax=35.0)
        
        #Add some useful text
        plt.figtext(0.72,0.9,"Chopper 1 Parameters",size='small', weight='bold')
        plt.figtext(0.73,0.84,self.doChopperPairOutputString(self.chopper1Dist,self.chopper1AOpen,self.chopper1BClose),size='small')
        plt.figtext(0.73,0.76,self.doChopperDiskOutputString(1,self.chopper1Opening,self.chopper1ADist,self.chopper1AOpen,self.chopper1AClose), size='small')
        plt.figtext(0.73,0.68,self.doChopperDiskOutputString(2,self.chopper1Opening,self.chopper1BDist,self.chopper1BOpen,self.chopper1BClose), size='small')
        
        plt.figtext(0.72,0.66,"Chopper 2 Parameters",size='small', weight='bold')
        plt.figtext(0.73,0.60,self.doChopperPairOutputString(self.chopper2Dist,self.chopper2AOpen,self.chopper2BClose),size='small')
        plt.figtext(0.73,0.52,self.doChopperDiskOutputString(1,self.chopper2Opening,self.chopper2ADist,self.chopper2AOpen,self.chopper2AClose), size='small')
        plt.figtext(0.73,0.44,self.doChopperDiskOutputString(2,self.chopper2Opening,self.chopper2BDist,self.chopper2BOpen,self.chopper2BClose), size='small')
        
        plt.figtext(0.72,0.42,"Wavelength Parameters", size='small', weight='bold')
        plt.figtext(0.73,0.36,"Full Pulse:\n  Min = {0:.2f} A\n  Max = {1:.2f} A".format(self.minL,self.maxL), size='small')
        plt.figtext(0.73,0.30,"Penumbra:\n  Min = {0:.2f} A\n  Max = {1:.2f} A".format(self.minLPen,self.maxLPen), size='small')

        plt.figtext(0.72,0.28,"Instrument Parameters", size='small',weight='bold')
        plt.figtext(0.73,0.22,"Collimation Start: {0:.2f} m\nSample Postion: {1:.2f} m\nRear Detector: {2:.2f} m".format(self.collStartDist,self.samplePosDist,self.rearDetDist), size='small')

        return fig
        
    

    def calcTOF(self,wavelength,detDist): 
        return wavelength*detDist/3.96
    
    def calcChopper(self,pos,minL,maxL):
        ''' Calculate chopper open and close'''
        chopOpen = self.calcTOF(minL,pos)
        chopClose = self.calcTOF(maxL,pos)+2.86
    
        return (chopOpen,chopClose)
    
    def calcChopperDisk1(self,pos,minL,openAngle,freq):
        ''' Calculate settings for first disk of a chopper pair '''
        chopOpen = self.calcTOF(minL,pos)
        chopClose = chopOpen+(openAngle*1000/freq/360)
    
        return (chopOpen,chopClose)     
    
    def calcChopperDisk2(self,pos,maxL,openAngle,freq):
        ''' Calculate settings for second disk of a chopper pair '''
        chopClose = self.calcTOF(maxL,pos)+2.86
        chopOpen = chopClose-(openAngle*1000/freq/360)
        
        return (chopOpen,chopClose)         
    
    def calcLambda(self,startTime,tof,dist):
        ''' Calculate Wavelength that will pass chopper '''
    
        return (tof-startTime)*3.96/dist
    
    def doChopperPairOutputString(self,cDist,cOpen,cClose):
    
        outstr = "Open Time = {0:.2f} ms\nPosition = {1:.2f} m\n".format(cClose-cOpen,cDist)
    
        return outstr
    
    def doChopperDiskOutputString(self,diskNo,angle,cDist,cOpen,cClose):
    
        outstr = "Disk {0}\nOpen Time = {1:.2f} ms\nOpen Angle = {2:.2f} deg\nPosition = {3:.2f} m\n".format(diskNo,cClose-cOpen,angle,cDist)
    
        return outstr

if __name__ == '__main__':

    for extralen in (0.0,0.5,1.0,2.0,3.0):
        cDist = 10.0
        rDist = 10.0
        tDist = cDist+10.0+rDist
        chopsys1 = chopperSystem(minL=3.0,maxL=10.5*tDist/(tDist+extralen),chop1Dist=6.7,chop2Dist=9.9,collDist=cDist+extralen,pulseSkip=0,rearDist=rDist)
        chopsys1.doChopperCalcs()
        chopsys1.doLambdaCalcs()
        tdplot1 = chopsys1.doTDPlot()
    
    plt.show()


    
    
    
    
