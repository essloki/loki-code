#!/usr/bin/python -u

import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import matplotlib.gridspec as gridspec
plt.ioff()

def calcTOF(wavelength,detDist): 
    return wavelength*detDist/3.96

def calcChopper(pos,minL,maxL):
    ''' Calculate chopper open and close'''
    chopOpen = calcTOF(minL,pos)
    chopClose = calcTOF(maxL,pos)+2.86
    
    return (chopOpen,chopClose)

def calcLambda(startTime,tof,dist):
    ''' Calculate Wavelength that will pass chopper '''
    
    return (tof-startTime)*3.96/dist


def doChopperOutputString(cDist,cOpen,cClose):
    
    outstr = "Open Time = {0:.2f} ms\nPosition = {1:.2f} m\n".format(cClose-cOpen,cDist)
    
    return outstr

outWidth = 11.7
outHeight = 8.3
oWS = 1/outWidth
oHS = 1/outHeight


chopper1Dist = 6.5
chopper2Dist = 9.8
rearDetDist = 30.0
minL = 3.0
maxL = 10

numPulses = 2

chopper1Open,chopper1Close = calcChopper(chopper1Dist,minL,maxL)
chopper1X1 = np.array([0,chopper1Open])
chopper1X2 = np.array([chopper1Close,chopper1Open+71.4])
chopper1Y = np.array([chopper1Dist,chopper1Dist])


chopper2Open,chopper2Close = calcChopper(chopper2Dist,minL,maxL)
chopper2X1 = np.array([0,chopper2Open])
chopper2X2 = np.array([chopper2Close,chopper2Open+71.4])
chopper2Y = np.array([chopper2Dist, chopper2Dist])

minLPen = calcLambda(2.86,chopper2Open,chopper2Dist)
maxLPen = calcLambda(0,chopper2Close,chopper2Dist)

print minLPen

minLX = np.array([0,calcTOF(minL,rearDetDist)])
minLY = np.array([0,rearDetDist])

maxLX = np.array([2.86,2.86+calcTOF(maxL,rearDetDist)])
maxLY = np.array([0,rearDetDist])

minLPenX = np.array([2.86,2.86+calcTOF(minLPen,rearDetDist)])
minLPenY = np.array([0,rearDetDist])

maxLPenX = np.array([0,calcTOF(maxLPen,rearDetDist)])
maxLPenY = np.array([0,rearDetDist])

#Setup
fig = plt.figure(figsize=(outWidth,outHeight))
gs = gridspec.GridSpec(1,2, width_ratios=[4,1]) #,height_ratios=[20,1])
plt0=plt.subplot(gs[0])

#Draw choppers
plt0.plot(chopper1X1,chopper1Y, 'k',chopper1X2,chopper1Y, 'k', chopper1X2+71.4, chopper1Y, 'k')
plt0.plot(chopper2X1,chopper2Y, 'k', chopper2X2,chopper2Y, 'k', chopper2X2+71.4, chopper2Y, 'k')

#Draw neutron trajectories
plt0.plot(minLX,minLY, 'r', maxLX,maxLY, 'b')
plt0.plot(minLX+71.4,minLY, 'r', maxLX+71.3, maxLY, 'b')
plt0.plot(minLPenX,minLPenY, 'r--', maxLPenX,maxLPenY, 'b--')
plt0.plot(minLPenX+71.4,minLPenY, 'r--', maxLPenX+71.4,maxLPenY, 'b--')

ax = plt.gca()
#Draw pulses
ax.add_patch(Rectangle((0,0),2.86,rearDetDist, facecolor="lightgrey"))
ax.add_patch(Rectangle((71.4,0),2.86,rearDetDist, facecolor="lightgrey"))
#Draw Detectors
plt0.axhline(rearDetDist, lw=1, ls='--', color='g')
plt0.axhline(25.0, lw=1, ls='--', color='g')
plt0.axhline(22.0, lw=1, ls='--', color='g')
#Draw Sample Position
plt0.axhline(20, lw=1, ls='-.', color='g')

plt0.set_xlabel('Time (ms)')
plt0.set_ylabel('Distance (m)')

plt0.set_ylim(ymax=rearDetDist+1.0)


#Add some useful text
plt.figtext(0.72,0.9,"Chopper 1 Parameters",size='small', weight='bold')
plt.figtext(0.73,0.84,doChopperOutputString(chopper1Dist,chopper1Open,chopper1Close), size='small')

plt.figtext(0.72,0.8,"Chopper 2 Parameters",size='small', weight='bold')
plt.figtext(0.73,0.74,doChopperOutputString(chopper2Dist,chopper2Open,chopper2Close), size='small')

plt.figtext(0.72,0.7,"Wavelength Parameters", size='small', weight='bold')
plt.figtext(0.73,0.64,"Full Pulse:\n  Min = {0:.2f} A\n  Max = {1:.2f} A".format(minL,maxL), size='small')
plt.figtext(0.73,0.58,"Penumbra:\n  Min = {0:.2f} A\n  Max = {1:.2f} A".format(minLPen,maxLPen), size='small')

plt.savefig('choppers.pdf')
plt.show()